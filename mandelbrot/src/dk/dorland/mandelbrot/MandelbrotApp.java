package dk.dorland.mandelbrot;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MandelbrotApp extends Application {
    private Button btnGogo = new Button("Gogo");

    private int width = 1000;
    private int height = 800;
    
    
    private double x_move = 0;
    private double y_move = 0;
    private double zoom = 1.0;

    public static void main(String args[]) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        GridPane rootPane = new GridPane();
        Canvas canvas = new Canvas(8000, 8000);
        rootPane.add(canvas, 0, 0);
        Scene scene = new Scene(rootPane, width, height);

        btnGogo.setOnAction(event -> {
            drawMandelbrot(canvas);
        });

        primaryStage.setTitle("Mandelbrot App!!!");
        primaryStage.setScene(scene);
        primaryStage.show();

        zoom = 1.0;
        drawMandelbrot(canvas);
        
        canvas.setOnMouseClicked(event -> {
            x_move = x_move + (width * 0.5 - event.getSceneX());
            y_move = y_move + height * 0.5 - event.getSceneY();
            
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                double factor = 1.75;
                zoom = zoom * factor;
                x_move = x_move * factor;
                y_move = y_move * factor;
            } else if (event.getButton().equals(MouseButton.SECONDARY)) {
                double factor = 0.80;
                zoom = zoom * (factor);
                x_move = x_move * (factor);
                y_move = y_move * (factor);
            }
            
            drawMandelbrot(canvas);
        });
        
        scene.widthProperty().addListener( (event, oldValue, newValue) -> {
            width = newValue.intValue();
        });
        
        scene.heightProperty().addListener( (event, oldValue, newValue) -> {
            height = newValue.intValue();
            Platform.runLater(() -> {
                drawMandelbrot(canvas);
            });
        });
    }
    

    public void drawMandelbrot(Canvas canvas) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        int max = 200;
        
        gc.hashCode();
        
        if (zoom > 4e9) {
            max = 1200;
        } else if (zoom > 60000) {
            max = 600;
        }
        
        int[] colors = new int[max];
        for (int i = 0; i < max; i++) {
            colors[i] = java.awt.Color.HSBtoRGB(0.9f + i / 512f, 1, i / (i + 8f));
        }
        


        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                double offset_x = width * 0.5 + x_move;
                double offset_y = height * 0.5 + y_move;
                
                double c_real = 4.0 * (col - offset_x) / (zoom * width);
                double c_imag = 4.0 * (row - offset_y) / (zoom * width);
                
                double x = 0, y = 0;
                int iteration = 0;
                while (x * x + y * y <= 4 && iteration < max) {
                    double x_new = x * x - y * y + c_real;
                    y = 2 * x * y + c_imag;
                    x = x_new;
                    iteration++;
                }

                if (iteration < max) {
                    gc.getPixelWriter().setArgb(col, row, colors[iteration]);
                } else {
                    gc.getPixelWriter().setArgb(col, row, colors[0]);
                }
            }
        }

    }

}
